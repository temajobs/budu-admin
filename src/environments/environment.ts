const _options = {
  domain: 'http://local.budu.jobs',
  uploadsPath: '/uploads'
};

export const environment = {
  production: false,
  domain: _options.domain,
  uploadsLogosPath: `${_options.domain}${_options.uploadsPath}/logos`,
  api: {
    endpoint: `${_options.domain}/api/v1`
  }
};
