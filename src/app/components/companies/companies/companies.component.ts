import { Component, OnInit, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {

  dataSource: any[];
  endpoint: string = '/vacancies/employer';

  filtersApply: EventEmitter<any> = new EventEmitter();

  columnConfig = {
   '_id': 'ID', 
   'vacancy__post': 'Название вакансии', 
   'company__title': 'Название компании', 
   'created': 'Дата отправки на модерацию', 
   'status': 'Статус вакансии',
  };

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
  }

}
