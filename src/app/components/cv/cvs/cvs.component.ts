import { Component, OnInit, EventEmitter } from "@angular/core";

@Component({
  selector: "app-cvs",
  templateUrl: "./cvs.component.html",
  styleUrls: ["./cvs.component.scss"]
})
export class CvsComponent implements OnInit {
  dataSource: any[];
  filteredDataSource: any[];
  endpoint = "/admin/cv/";

  columnConfig = {
    _id: "ID",
    personal__firstName: "Имя",
    personal__lastName: "Фамилия",
    created: "Дата регистрации",
    updated: "Дата обновления профиля",
    responses: "Откликов",
    views: "Просмотров",
    email: "E-mail",
    phone: "Телефон",
    status: "Статус"
  };

  filters = {
    published: "Опубликованные",
    unpublished: "Неопубликованные"
  };

  filtersApply: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.filteredDataSource = this.dataSource;
  }

  filtersApplyController(obj) {
    this.filtersApply.emit(obj);
  }
}
