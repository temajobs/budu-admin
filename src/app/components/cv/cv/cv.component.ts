import { Component, OnInit } from "@angular/core";
import { Location } from "@angular/common";
import { ActivatedRoute } from "@angular/router";

import * as moment from "moment";

import { ApiService } from "src/app/services/api.service";

@Component({
  selector: "app-cv",
  templateUrl: "./cv.component.html",
  styleUrls: ["./cv.component.scss"]
})
export class CvComponent implements OnInit {
  responses: any;

  id: any;
  entry: any;
  endpoint: string = "admin/cv";

  public moment = moment;

  constructor(
    protected route: ActivatedRoute,
    protected _location: Location,
    protected api: ApiService
  ) {
    this.moment.locale("ru");
  }

  ngOnInit() {
    this.responses = {
      columnConfig: {
        'created': 'Дата',
        'company__title': 'Компания',
        'vacancy__post': 'Вакансия',
        'isViewed': 'Просмотрен',
      },
      dataSource: {

      },
      endpoint: '',
    };

    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    });

    if (!this.entry) {
      this.getById().then((res) => {
        this.entry = res;
        // TODO: наладить обработку данных отсюда или поменять выдачу в эндпоинте
        this.responses.endpoint = '/admin/responses/worker/' + this.entry._id + '/';
      });
    }
  }

  getById() {
    return this.api.getEntry(this.endpoint, this.id);
  }

  // TODO: после срабатывания любого из событий страничка должна обновляться, кнопка изменяться
  publish($event) {
    this.api.makeCvPublic(this.id);
  }

  change($event) {
    this.api.makeCvRejected(this.id);
  }

  block($event) {
    this.api.makeCvBlocked(this.id);
  }

  resetPassword($event) {
    this.api.sendRecoveryEmail(this.id);
  }
}
