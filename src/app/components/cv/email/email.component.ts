import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.scss']
})
export class EmailComponent implements OnInit {

  letter: FormControl = new FormControl('', [Validators.required, Validators.email]);

  @Input()
  id: any;

  constructor(
    private api: ApiService,
  ) { }

  ngOnInit() {
  }

  sendEmail() {
    this.api.sendCVDisapproveEmail(this.id, this.letter.value).then((resp) => {
      console.log('sent', resp.body);
    });
  }

}
