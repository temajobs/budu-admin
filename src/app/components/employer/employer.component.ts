import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-employer',
  templateUrl: './employer.component.html',
  styleUrls: ['./employer.component.scss']
})
export class EmployerComponent implements OnInit {

  selectedSection: string = '';
  // ключи же и эндпоинты
  sections = {
    'companies': 'Компании',
    'vacancies': 'Вакансии',
    'payments': 'Оплаты',
  }

  constructor(
    private location: Location,
  ) { }

  ngOnInit() {
  }

}
