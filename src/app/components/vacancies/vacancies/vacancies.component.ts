import { Component, OnInit, EventEmitter, Input } from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-vacancies",
  templateUrl: "./vacancies.component.html",
  styleUrls: ["./vacancies.component.scss"]
})
export class VacanciesComponent implements OnInit {
  responses: any;
  statuses: any;

  dataSource: any[];
  filteredDataSource: any[];
  endpoint: string = "/vacancies/employer";

  filters = {
    published: "Опубликованные",
    unpublished: "Неопубликованные"
  };

  filtersApply: EventEmitter<any> = new EventEmitter();

  columnConfig = {
    _id: "ID",
    vacancy__post: "Название вакансии",
    company__title: "Название компании",
    created: "Дата отправки на модерацию",
    status: "Статус вакансии"
  };

  constructor(
    private api: ApiService,
  ) {}

  ngOnInit() {
    this.filteredDataSource = this.dataSource;

    this.api.getStatuses('vacancy').then(res => {
      this.statuses = res;
    });
  }

  filtersApplyController($event) {
    this.filtersApply.emit($event);
  }
}
