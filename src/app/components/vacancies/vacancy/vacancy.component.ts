import { Component, OnInit } from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import { ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";

import * as moment from "moment";

@Component({
  selector: "app-vacancy",
  templateUrl: "./vacancy.component.html",
  styleUrls: ["./vacancy.component.scss"]
})
export class VacancyComponent {
  responses: any;

  id: any;
  entry: any;
  endpoint: string = "vacancies/vacancy";

  public moment = moment;

  constructor(
    protected route: ActivatedRoute,
    protected _location: Location,
    protected api: ApiService
  ) {
    this.moment.locale("ru");
  }

  ngOnInit() {
    this.responses = {
      columnConfig: {
        personal__firstName: "Имя соискателя",
        created: "Дата отклика",
        isFavourite: "В избранном",
        isRefused: "Отказано",
        isViewed: "Просмотрено",
        updated: "Дата последнего изменения"
      },
      dataSource: {},
      endpoint: ""
    };

    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    });

    if (!this.entry) {
      this.getById().then(res => {
        this.entry = res;
        this.responses.endpoint = "/admin/vacancy/" + this.entry._id + "/";
      });
    }
  }

  getById() {
    return this.api.getEntry(this.endpoint, this.id);
  }

  // TODO: после срабатывания любого из событий страничка должна обновляться, кнопка изменяться
  publish($event) {
    this.api.makeVacancyPublic(this.id);
  }

  change($event) {
    this.api.makeVacancyRejected(this.id);
  }

  block($event) {
    this.api.makeVacancyBlocked(this.id);
  }

  resetPassword($event) {
    this.api.sendRecoveryEmail(this.id);
  }
}
