import { Component, OnInit } from '@angular/core';
import { Location } from "@angular/common";

import * as moment from "moment";
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  id: any;

  entry: any;
  endpoint: string = "admin/user/view";

  public moment = moment;

  constructor(
    protected route: ActivatedRoute,
    protected _location: Location,
    protected api: ApiService
  ) {
    this.moment.locale("ru");
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    });

    if (!this.entry) {
      this.getById().then((res) => {
        this.entry = res;
      });
    }
  }

  getById() {
    return this.api.getEntry(this.endpoint, this.id);
  }

  activate($event) {
    this.api.makeUserActive(this.id);
  }

  resetPassword($event) {
    this.api.sendRecoveryEmail(this.id);
  }

  block($event, duration: string = 'full') {
    this.api.makeUserBlocked(this.id, duration);
  }

}
