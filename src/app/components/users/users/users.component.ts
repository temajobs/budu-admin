import { Component, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  dataSource: any[];
  filteredDataSource: any[];
  endpoint = "/admin/user/";

  columnConfig = {
    _id: "ID",
    created: "Дата регистрации",
    updated: "Дата обновления профиля",
    email: "E-mail",
  };

  filters = {
    admin: "Админ",
    user: "Обычный пользователь"
  };

  filtersApply: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.filteredDataSource = this.dataSource;
  }

  filtersApplyController(obj) {
    this.filtersApply.emit(obj);
  }

}
