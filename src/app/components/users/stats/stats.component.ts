import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-stats",
  templateUrl: "./stats.component.html",
  styleUrls: ["./stats.component.scss"]
})
export class StatsComponent implements OnInit {
  @Input()
  entry: any;

  config: any = {
    _id: "ID",
    created: "Дата регистрации",
    modified: "Дата обновления",
    last_visit: "Дата последнего визита",
    responses: "Откликов всего",
    views: "Просмотров",
    favourites: "Избранных вакансий всего",
    // TODO: status-ы для юзеров показывать в статистике
  };

  constructor() {}

  ngOnInit() {}
}
