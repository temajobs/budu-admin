import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class RequestService implements HttpInterceptor {
    constructor(
      private dialog: MatDialog,
      private snackBar: MatSnackBar
      ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request).pipe(
        map((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {
              const snackBarRef = this.snackBar.open(event.body['message'] || 'Всё хорошо ' + event.status, 'Понятно');
            }
            return event;
        },
        catchError(err => {
          const msg = err.error;
          if (err.status == 400)  {
            alert("Произошла ошибка во взаимодействии клиента и сервера");
          } else if (err.status === 500) {
            alert("Произошла ошибка на сервере");
          }
          const snackBarRef = this.snackBar.open('Ошибка ' + err.status, 'Понятно');

          const error = err.statusText || (err.error && err.error.message);
          return throwError(error);
      })));
    }
}
