import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    api = environment.api.endpoint;

    constructor(
      @Inject(HttpClient) private http: HttpClient
    ) { }
  
    /**
     * Get entries by endpoint
     * @param versity_name string
     */
    getEntries(endpoint: string): Promise<any> {
      return this.http.get(`${this.api}${endpoint}`)
      .pipe(catchError(this.handleError)).toPromise();
    }

    /**
     * Get entry by endpoint and id
     * @param endpoint string
     * @param id string
     */
    getEntry(endpoint: string, id: string): Promise<any> {
      return this.http.get(`${this.api}/${endpoint}/${id}`)
      .pipe(catchError(this.handleError)).toPromise();
    }

    makeCvPublic(id: string) {
      return this.changeCvStatus(id, 'published');
    }

    makeCvRejected(id: string) {
      return this.changeCvStatus(id, 'rejected');
    }

    makeCvBlocked(id: string) {
      return this.changeCvStatus(id, 'blocked');
    }

    makeVacancyPublic(id: string) {
      return this.changeVacancyStatus(id, 'published');
    }

    makeVacancyRejected(id: string) {
      return this.changeVacancyStatus(id, 'rejected');
    }

    makeVacancyBlocked(id: string) {
      return this.changeVacancyStatus(id, 'blocked');
    }

    makeUserActive(id: string) {
      return this.changeUserStatus(id, 'active');
    }

    makeUserBlocked(id: string, duration: string) {
      return this.changeUserStatus(id, 'blocked');
    }

    resetUserPassword(id: string) {
      return 
    }

    /**
     * Change vacancy status 
     * @param id string
     * @param status string
     */
    changeVacancyStatus(id: string, status: string): Promise<any> {
      return this.http.patch(`${this.api}/vacancies/vacancy/status/${status}/${id}`, {})
      .pipe(catchError(this.handleError)).toPromise();
    }

    /**
     * Change CV status 
     * @param id string
     * @param status string
     */
    changeCvStatus(id: string, status: string): Promise<any> {
      return this.http.patch(`${this.api}/admin/cv/status/${status}/${id}`, {})
      .pipe(catchError(this.handleError)).toPromise();
    }

    /**
     * Change user status 
     * @param id string
     * @param status string
     */
    changeUserStatus(id: string, status: string): Promise<any> {
      return this.http.patch(`${this.api}/admin/user/status/${status}/${id}`, {})
      .pipe(catchError(this.handleError)).toPromise();
    }

    /** 
     * Send disapprove email to CV publisher
     * @param id string
     * @param text string
     */
    sendCVDisapproveEmail(userid: string, text: string): Promise<any> {
        return this.http.post(`${this.api}/admin/cv/email/${userid}`, { text: text })
        .pipe(catchError(this.handleError)).toPromise();
    }
  
    /** 
     * Send password recovery email to user
     * @param id string
     * @param text string
     */
    sendRecoveryEmail(email: string): Promise<any> {
      return this.http.post(`${this.api}/admin/recovery/`, { email: email })
      .pipe(catchError(this.handleError)).toPromise();
    }

    /**
     * Get statuses object
     * @param versity_name string
     */
    getStatuses(endpoint: string): Promise<any> {
      return this.http.get(`${this.api}/admin/status_list/${endpoint}`)
      .pipe(catchError(this.handleError)).toPromise();
    }

    /**
     * Error Handler
     */
    protected handleError(error: HttpErrorResponse) {
      return throwError({});
    }

}
