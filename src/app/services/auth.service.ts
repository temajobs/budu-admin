import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private api = `${environment.api.endpoint}/account`;
  private accountSubject$: BehaviorSubject<any>;

  constructor(private _http: HttpClient) {
    this.accountSubject$ = new BehaviorSubject(null);
  }

  public fetchAccount() {
    return this._http.get(`${this.api}`)
      .pipe(tap((res: any) => this.accountSubject$.next(res.account)));
  }

  public getCurrentUser(): Observable<any> {
    return this.accountSubject$.asObservable();
  }

  public isLogged(): Observable<boolean> {
    return this.getCurrentUser()
      .pipe(tap(_ => console.log('isLogged')))
      .pipe(tap(console.log))
      .pipe(map(account => account ? true : false));
  }

  public isEmployer(): Observable<boolean> {
    return this.getCurrentUser()
      .pipe(tap(account => console.log(account.role)))
      .pipe(map(account => account && account.role === 1 ? true : false));
  }

  public isWorker(): Observable<boolean> {
    return this.getCurrentUser()
      .pipe(tap(account => console.log(account.role)))
      .pipe(map(account => account && account.role === 0 ? true : false));
  }

}
