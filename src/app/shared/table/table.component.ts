import { Component, OnInit, Input, EventEmitter } from "@angular/core";
import { ApiService } from "src/app/services/api.service";
import { Router, ActivatedRoute } from "@angular/router";

import * as moment from "moment";
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: "app-table",
  templateUrl: "./table.component.html",
  styleUrls: ["./table.component.scss"]
})
export class TableComponent implements OnInit {
  path: string;

  columns: string[];
  columnsToDisplay: string[];

  @Input()
  columnConfig: any;

  @Input()
  statuses: any;

  @Input()
  dataSource: any[];
  
  @Input()
  filtersApply: EventEmitter<any>;
  
  @Input()
  endpoint: string;

  filteredDataSource: any[];

  fullPath: any;

  window: MatTableDataSource<any>

  constructor(
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.columns = Object.keys(this.columnConfig);
    this.columnsToDisplay = this.columns;

    this.api.getEntries(this.endpoint).then(resp => {
      this.dataSource = this.deserializeDS(resp);
      this.filteredDataSource = this.dataSource;
      this.window = new MatTableDataSource(this.filteredDataSource.slice(0, 5));
    });

    if (this.filtersApply) {
      this.filtersApply.subscribe(filters => {
        this.filteredDataSource = this.dataSource.filter(e => this.filterByQuery(e, filters));
        this.filteredDataSource.filter = filters["filterString"];
      });
    }

    this.route.url.subscribe((value) => {
      this.fullPath = value;
      this.path = value[0].path;
    });
  }

  navigateTo(entry) {
    var id = entry._id;
    if (this.path == "cv" && this.fullPath.length == 2 && entry._vId) {
      id = entry._vId;
    } else if (this.path == "vacancies"  && this.fullPath.length == 2 && entry._aId) {
      id = entry._aId;
    }
    this.router.navigate([id], { relativeTo: this.route })
  }

  filterByQuery(e, filters) {
    for (let key in filters) {
      if (
        key == "startDate" &&
        moment(filters["startDate"]).isAfter(e["updated"])
      ) {
        return false;
      } else if (
        key == "finishDate" &&
        moment(filters["finishDate"]).isBefore(e["updated"])
      ) {
        return false;
      }
    }
    return true;
  }

  deserializeDS(ds) {
    var newDS = [];
    ds.forEach(e => {
      newDS.push(this.prepareDataSource(e));
    });
    return newDS;
  }

  prepareDataSource(p) {
    var newObj = {};
    for (var key in p) {
      if (p.hasOwnProperty(key) && p[key] && typeof p[key] == "object") {
        for (var in_key in p[key]) {
          if (p[key].hasOwnProperty(in_key)) {
            newObj[key + "__" + in_key] = p[key][in_key];
          }
        }
      } else {
        newObj[key] = p[key];
      }
    }
    return newObj;
  }

  updateWindow($event) {
    let sliceTo = [$event.pageSize * ($event.previousPageIndex + 1), $event.pageSize * ($event.pageIndex + 1)];
    if ($event.previousPageIndex > $event.pageIndex) {
      sliceTo = [$event.pageSize * $event.pageIndex, $event.pageSize * $event.previousPageIndex ];
    }
    this.window = new MatTableDataSource(this.filteredDataSource.slice(sliceTo[0], sliceTo[1]));
  }
}
