import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.scss']
})
export class TableFilterComponent implements OnInit {

  filterStartDate: FormControl = new FormControl();
  filterFinishDate: FormControl = new FormControl();
  filterString: FormControl = new FormControl();

  dataSource: any[];
  filteredDataSource: any[];

  @Output()
  filtersApply: EventEmitter<any> = new EventEmitter(); 

  filtersApplied: any = {};

  @Input()
  filters: any;

  constructor() { }

  ngOnInit() {
    this.filteredDataSource = this.dataSource;

    this.filterStartDate.valueChanges.subscribe((val) => {
      this.applyFilterToDataSource({'startDate': val});
    });

    this.filterFinishDate.valueChanges.subscribe((val) => {
      this.applyFilterToDataSource({'finishDate': val});
    });

    this.filterString.valueChanges.subscribe((val) => {
      this.applyFilterToDataSource({'filterString': val});
    });  
    
  }

  applyFilterToDataSource(patch: any) {
    this.filtersApplied = {
      ...this.filtersApplied,
      ...patch,
    }

    this.filtersApply.emit(
      this.filtersApplied
    );
  }

}
