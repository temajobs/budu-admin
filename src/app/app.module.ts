import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './containers/header/header.component';
import { FooterComponent } from './containers/footer/footer.component';
import { NgModule } from '@angular/core';
import { TableComponent } from './shared/table/table.component';
import { VacanciesComponent } from './components/vacancies/vacancies/vacancies.component';
import { VacancyComponent } from './components/vacancies/vacancy/vacancy.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModerationBarComponent } from './containers/moderation-bar/moderation-bar.component';
import { CvsComponent } from './components/cv/cvs/cvs.component';
import { CvComponent } from './components/cv/cv/cv.component';
import { EmployerComponent } from './components/employer/employer.component';
import { StatsComponent } from './components/users/stats/stats.component';
import { CompaniesComponent } from './components/companies/companies/companies.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { EmailComponent } from './components/cv/email/email.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { PaymentComponent } from './components/payments/payment/payment.component';
import { UserComponent } from './components/users/user/user.component';
import { UsersComponent } from './components/users/users/users.component';
import { TableFilterComponent } from './shared/table-filter/table-filter.component';
import { RequestService } from './services/request.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    TableComponent,
    VacanciesComponent,
    VacancyComponent,
    ModerationBarComponent,
    CvsComponent,
    CvComponent,
    EmployerComponent,
    StatsComponent,
    CompaniesComponent,
    PaymentsComponent,
    EmailComponent,
    PaymentComponent,
    UserComponent,
    UsersComponent,
    TableFilterComponent,
  ],
  imports: [
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  providers: [
    MatDatepickerModule,
    { provide: HTTP_INTERCEPTORS, useClass: RequestService, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
