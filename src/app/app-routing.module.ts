import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VacanciesComponent } from './components/vacancies/vacancies/vacancies.component';
import { EmployerComponent } from './components/employer/employer.component';
import { VacancyComponent } from './components/vacancies/vacancy/vacancy.component';
import { CvsComponent } from './components/cv/cvs/cvs.component';
import { CvComponent } from './components/cv/cv/cv.component';
import { CompaniesComponent } from './components/companies/companies/companies.component';
import { UsersComponent } from './components/users/users/users.component';
import { UserComponent } from './components/users/user/user.component';


const routes: Routes = [
  {
    path: 'employer',
    component: EmployerComponent,
    children: [
      {
        path: '', 
        redirectTo: 'vacancies', 
        pathMatch: 'full'}, 
      {
        path: 'vacancies',
        component: VacanciesComponent,
      },
      {
        path: 'vacancies/:id',
        component: VacancyComponent,
      },
      {
        path: 'vacancies/:vcid/:id',
        component: CvComponent,
      },
      {
        path: 'companies',
        component: CompaniesComponent,
      },
      {
        path: 'payments',
        component: CompaniesComponent,
      },
  ]},
  {
    path: 'worker',
    // component: WorkerComponent,
    children: [
      {
        path: '', 
        redirectTo: 'cv', 
        pathMatch: 'full'}, 
      {
        path: 'cv',
        component: CvsComponent,
      },
      {
        path: 'cv/:id',
        component: CvComponent,
      },
      {
        path: 'cv/:cvid/:id',
        component: VacancyComponent,
      },
  ]},
  {
    path: 'users',
    component: UsersComponent,
  },
  {
    path: 'users/:id',
    component: UserComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
