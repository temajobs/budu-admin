import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-moderation-bar',
  templateUrl: './moderation-bar.component.html',
  styleUrls: ['./moderation-bar.component.scss']
})
export class ModerationBarComponent implements OnInit {
  
  @Input()
  endpoint: string;

  @Input()
  object: any;

  @Output()
  published: EventEmitter<any> = new EventEmitter();

  @Output()
  changed: EventEmitter<any> = new EventEmitter();

  @Output()
  blocked: EventEmitter<any> = new EventEmitter();

  @Output()
  resetPassword: EventEmitter<any> = new EventEmitter();
  
  id: any;
  path: string;

  config: any = {
    blockable: {
      'users': true,
      'cv': true,
      'vacancies': true,
    },
    changeable: {
      'cv': true,
      'vacancies': true,
    },
    publishable: {
      'cv': true,
      'vacancies': true,
    },
    resetable: {
      'users': true,
      'cv': true,
      'vacancies': true,
    }
  }

  constructor(
    protected route: ActivatedRoute,
    protected api: ApiService,
    protected _location: Location,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get("id");
    });

    if (!this.object) {
      this.getById().then((res) => {
        this.object = res;
      });
    }

    this.route.url.subscribe((value) => {
      this.path = value[0].path;
    });
  }

  getById() {
    return this.api.getEntry(this.endpoint, this.id);
  }

  publish($event) {
    this.published.emit($event);
  }

  change($event) {
    this.changed.emit($event);
  }

  block($event) {
    this.blocked.emit($event);
  }

  reset($event) {
    this.resetPassword.emit($event);
  }

}
