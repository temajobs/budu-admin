import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModerationBarComponent } from './moderation-bar.component';

describe('ModerationBarComponent', () => {
  let component: ModerationBarComponent;
  let fixture: ComponentFixture<ModerationBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModerationBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModerationBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
