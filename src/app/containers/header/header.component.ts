import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public account$: Observable<any>;

  constructor(private authService: AuthService) {
    this.account$ = new Observable<any>();
  }

  ngOnInit() {
    this.account$ = this.authService.getCurrentUser();
  }

}
